import {createElement} from "../../helpers/domHelper";
import {showModal} from "./modal";

export function showWinnerModal(fighter) {
    let {name, source} = fighter;
    let img = document.createElement('img');
    img.src = `${source}`;
    img.style.height = 380 + "px";
    let WinnerModal = {
        title: name,
        bodyElement: img
    }
    showModal(WinnerModal)

  // call showModal function 
}
