import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  let lhealth, lattack;
  let rhealth, rattack;
  for (const prop in firstFighter) {
    if (prop === "health") {
      lhealth = firstFighter[prop];
    }
    if (prop === "attack") {
      lattack = firstFighter[prop];
    }
  }
  for (const prop in secondFighter) {
    if (prop === "health") {
      rhealth = secondFighter[prop];
    }
    if (prop === "attack") {
      rattack = firstFighter[prop];
    }
  }
  let process = true;
  const lStartHealth = lhealth;
  const rStartHealth = rhealth;
  let off = document.getElementsByClassName("arena___health-bar")[0].offsetWidth;
  let rminusBar = off/rhealth;
  let lminusBar = off/lhealth;
  let aleftFighter = true;
  let arightFighter = true;
  let dleftFighter = true;
  let drightFighter = true;
  let arrBut = [];
  let tensecleft = true
  let tensecright = true
  document.onkeydown = function(e) {
    if (process) {
      switch (e.code) {
        case `${controls.PlayerOneAttack}`:
          if (!dleftFighter) break
          if (aleftFighter && drightFighter) {
            rhealth -= getDamage(firstFighter, secondFighter);
          }
          aleftFighter = false
          break
        case `${controls.PlayerTwoAttack}`:
          if (!drightFighter) break
          if (arightFighter && dleftFighter) {
            lhealth -= getDamage(secondFighter, firstFighter);
          }
          arightFighter = false
          break
        case `${controls.PlayerOneBlock}`:
          dleftFighter = false
          break
        case `${controls.PlayerTwoBlock}`:
          drightFighter = false
          break;
      }
      if (e.repeat) {
        return;
      }
      arrBut.push(e.code)
      let critflug = true;
      for (let v of controls.PlayerOneCriticalHitCombination) {
        if (!arrBut.includes(v)) {
          critflug = false;
          break;
        }
      }
      if (critflug && tensecleft && dleftFighter) {
        tensecleft = false;
        rhealth -= lattack * 2;
        setTimeout(() => {
          tensecleft = true;
        }, 10000)
      }
      critflug = true;
      for (let v of controls.PlayerTwoCriticalHitCombination) {
        if (!arrBut.includes(v)) {
          critflug = false;
          break;
        }
      }
      if (critflug && tensecright && drightFighter) {
        tensecright = false;
        lhealth -= rattack * 2;
        setTimeout(() => {
          tensecright = true;
        }, 10000)
      }
      if (rStartHealth !== rhealth) {
        if (rhealth > 0) {
          document.getElementsByClassName("arena___health-bar")[1].style.width = rhealth * rminusBar + "px";
        } else {
          document.getElementsByClassName("arena___health-bar")[1].style.width = 0 + "px";
        }
      }
      if (lStartHealth !== lhealth) {
        if (lhealth > 0) {
          document.getElementsByClassName("arena___health-bar")[0].style.width = lhealth * lminusBar + "px";
        } else {
          document.getElementsByClassName("arena___health-bar")[0].style.width = 0 + "px";
        }
      }
    }
  }
  document.onkeyup = function(e) {
    if (process) {
      switch (e.code) {
        case `${controls.PlayerOneAttack}`:
          aleftFighter = true
          break
        case `${controls.PlayerTwoAttack}`:
          arightFighter = true
          break
        case `${controls.PlayerOneBlock}`:
          dleftFighter = true
          break
        case `${controls.PlayerTwoBlock}`:
          drightFighter = true
          break;
      }
      if (arrBut.length === 0) {
        return;
      }
      arrBut.length = 0;
    }
  }
  return new Promise((resolve) => {
    setInterval(() => {
    if (lhealth <= 0) {
      process = false;
      resolve(secondFighter)
    } else if (rhealth <= 0) {
      process = false;
      resolve(firstFighter)
    }
  }, 100);
  })
}

export function getDamage(attacker, defender) {
  return Math.max(getHitPower(attacker) - getBlockPower(defender), 0);
  // return damage
}

export function getHitPower(fighter) {
  let {attack} = fighter;
  let criticalHitChance = Math.random()+1;
  return attack*criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  let {defense} = fighter;
  let dodgeChance = Math.random()+1;
  return defense*dodgeChance;
  // return block power
}
