import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighterElement.style.height = 200+"px";
  if (fighter) {
    fighterElement.style.backgroundColor = '#F08080';
    let fightEntries = Object.entries(fighter);
    fighterElement.style.border = "5px solid black"
    fightEntries.filter((el, index) => index > 0 && index < 5 ? fighterElement.innerHTML  += `<srtong>${el[0]}: ${el[1]}</srtong></br>` : true)
    let img = new Image();
    img.src = `${fightEntries[5][1]}`;
    if (positionClassName !== 'fighter-preview___left') {
      img.style.transform = "scale(-1, 1)"
    }
    fighterElement.appendChild(img);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
